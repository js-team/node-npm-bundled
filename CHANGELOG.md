# Changelog

## [2.0.1](https://github.com/npm/npm-bundled/compare/v2.0.0...v2.0.1) (2022-08-25)


### Dependencies

* bump npm-normalize-package-bin from 1.0.1 to 2.0.0 ([#13](https://github.com/npm/npm-bundled/issues/13)) ([aec07c1](https://github.com/npm/npm-bundled/commit/aec07c1fff4dd0690e3792c6fe00b6d7e574c017))

## [2.0.0](https://github.com/npm/npm-bundled/compare/v1.1.2...v2.0.0) (2022-08-22)


### ⚠ BREAKING CHANGES

* This adds an engine field with support for node `^12.13.0 || ^14.15.0 || >=16.0.0`.

### Documentation

* fix incorrect example of sync usage ([#9](https://github.com/npm/npm-bundled/issues/9)) ([45ccdf4](https://github.com/npm/npm-bundled/commit/45ccdf4211e0552e3957fc6dd8134a6440a803c3))


### Dependencies

* @npmcli/template-oss@3.5.0 ([#10](https://github.com/npm/npm-bundled/issues/10)) ([3ea4848](https://github.com/npm/npm-bundled/commit/3ea48487c07992c9c589ee527423ef8e3e193a7c))
